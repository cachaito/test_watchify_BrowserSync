'use strict';
var http = require("http"),
    fs = require("fs"),
    port = 1337,
    jsonFileName = "env/mocked.json";

http.createServer(function(req, res) {
    res.setHeader("Content-Type", "application/json");
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
    res.end(JSON.stringify(fs.readFileSync(jsonFileName, {encoding: "utf8"})));
}).listen(port);
