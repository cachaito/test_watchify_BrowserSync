"use strict";
var config = module.exports = {
    root: document.getElementById("tableItem"),
    messages: {
        noFile: "Loaded file is empty",
        error: "There was error in loading file"
    }
};
