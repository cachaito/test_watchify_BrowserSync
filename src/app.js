"use strict";
var m = require("mithril"),
    config = require("./config");

m.route.mode = "hash";
m.route(config.root, "/", {
    "/": require("./pages/Home.js")
});
