"use strict";
var m = require("mithril"),
    config = require("../config"),
    TableData = require("../models/TableData"),
    Home = {};

Home.vm = {
    init: function() {
        this.recivedData = m.prop([]),
        this.data = TableData.getData("http://localhost:1337")
            .then(function(data) {

                if(!data.length) {
                    throw new Error(config.messages.emptyFile);
                }

                Home.vm.recivedData(JSON.parse(data));
            }, function(err) {
                throw new Error(config.messages.error, + "\n" + err);
            })
    }
};

Home.controller = function() {
    Home.vm.init();
}

Home.view = function() {
    return [
      m("div", {}, "Some header :-) ", Home.vm.recivedData().map(function(elem) { //ctrl.recivedData().map(function(elem) {
        console.log(elem.balance);
        return m("p", {}, elem.balance);
      }))
    ];
};

module.exports = Home;

/**
*
* In comment I left previous only controller version (without Home.vm)
*
**/

// module.exports = Home;

// "use strict";
// var m = require("mithril"),
//     config = require("../config"),
//     TableData = require("../models/TableData");

// var Home = module.exports = {

//     controller: function() {
//         var ctrl = this;

//         ctrl.recivedData = m.prop([]);

//         TableData.getData("http://localhost:1337")
//             .then(function(data) {

//                 if(!data.length) {
//                     throw new Error(config.messages.emptyFile);
//                 }

//                 ctrl.recivedData(JSON.parse(data));
//             }, function(err) {
//                 throw new Error(config.messages.error, + "\n" + err);
//             });
//     },
//     view: function(ctrl) {
//         return [
//           m("div", {}, "Some header :-)", ctrl.recivedData().map(function(elem) {
//             console.log(elem.balance);
//             return m("p", {}, elem.balance);
//           }))
//         ];
//     }
// };
