"use strict";
var m = require("mithril");

var TableData = module.exports = {

    getData: function(path) {
        return m.request({
            method: "GET",
            url: path
        });
    }
};
